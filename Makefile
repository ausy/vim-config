VIM_CONFIG := ${HOME}/.vim
VIM_PLUGIN := ${VIM_CONFIG}/autoload/plug.vim 
SRC_DIR := rc.d
SRCS := ${shell find ${SRC_DIR} -name '*rc'}
OBJS := ${SRCS:${SRC_DIR}/%=${VIM_CONFIG}/%}

all: ${VIM_PLUGIN} ${OBJS}
config: ${OBJS}

${VIM_PLUGIN}:
	curl -sfLo ${VIM_PLUGIN} --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

${OBJS}:
	cp ${@:${VIM_CONFIG}/%=${SRC_DIR}/%} $@

.PHONY: clean
clean:
	rm -f ${OBJS} ${VIM_PLUGIN}
