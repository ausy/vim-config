# A list of possible intresting plugins not used in pluginsrc file.

| Plugin | Description |
| ---------- | ---------- |
| tpope/vim-sensible | Set some sensible defaults |
| tpope/vim-vinegar | Improve built in directory browser |
| mbbill/undotree | Undo history tree |
| scrooloose/nerdtree | Better file explorer |
| mbbill/undotree | Better undo history |
| ctrlpvim/ctrlp.vim | Easy way to open files and files |
| szw/vim-tags | Easy way to manage tags |
| xolox/vim-easytags |
| craigemery/vim-autotag |
| majutsushi/tagbar | A tag outliner |
| rking/ag.vim | Search on steroids. Silversearcher-ag needs to be installed |
| skwp/greplace.vim | Replace throughout your whole project |
| MarcWeber/vim-addon-mw-utils |
| tomtom/tlib_vim |
| tpope/vim-surround |
| StanAngeloff/php.vim |
| arnaud-lb/vim-php-namespace |
| stephpy/vim-php-cs-fixer |
| tobyS/vmustache | Mustache template system for VIMScript. |
| tobyS/pdv | PHP Documentor for Vim. Requires vmustache and ultisnips. |
| SirVer/ultisnips | Create snippets |
| honza/vim-snippets | Snippets |
| ervandew/supertab |
| stephpy/vim-yaml | Yaml syntax simpler/faster than core yaml syntax |
| sheerun/vim-polyglot "A collection of language packs for Vim |
| vim-syntastic/syntastic |
| tpope/vim-commentary |
| tpope/vim-repeat |
| Raimondi/delimitMate | Endwise should be loaded after delimitMate, because delimitMate overwrites <CR> instead of adding its mapping. |
| tpope/vim-endwise  |
| tpope/vim-fugitive |
| tpope/vim-haml  |
| tpope/vim-ragtag |
| tpope/vim-unimpaired |
| Yggdroot/indentLine |
| jeffkreeftmeijer/vim-numbertoggle |
| rstacruz/sparkup |
| elzr/vim-json |
| vim-ruby/vim-ruby |
| tpope/vim-rails |
| tpope/vim-projectionist |
| tpope/vim-rake "depends on vim-projectionist |
| tpope/vim-bundler |
| jlanzarotta/bufexplorer "Better buffers |
| jwalton512/vim-blade |
| vim-scripts/vim-auto-save |
| t9md/vim-ruby-xmpfilter |
| chrisbra/csv.vim |
| keith/tmux.vim |
| stephpy/vim-yaml |
| tmhedberg/SimpylFold |
| vim-scripts/indentpython.vim |
| nvie/vim-flake8 |
| istepura/vim-toolbar-icons-silk |
| NLKNguyen/papercolor-theme | find colorschemes on https://vimcolorschemes.com |
