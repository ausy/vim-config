# Vim Installation.

If you want the latest and greatest:

```shell
sudo add-apt-repository ppa:jonathonf/vim
sudo apt update
sudo apt install vim
```

# Vim Configuration.

## Install configuration files and plugin manager.

Clone the repository and run `make`.

```shell
git clone git@bitbucket.org:ausybenelux/vim-config.git
cd git-config
make
```

To update your existing configuration run make with the -B flag and the config target.

```shell
make -B config
```

## Install plugins.

Open vim and run `:PlugInstall`.

## Plugins dependencies.

### Tagbar

Linux:
```shell
snap install universal-ctags
```

macOS:
```shell
brew install --HEAD universal-ctags/universal-ctags/universal-ctags
```

### Easycomplete

Open vim and run `:InstallLspServer tabnine`.

## Custom configuration.

You can add or overwrite existing configuration inside a $VIM_HOME/myrc file.
